filetype plugin indent on
syntax on
set number
set sts=2
set listchars=tab:→\ ,trail:·,extends:…,eol:⏎,space:␣
nnoremap <F3> :set list!<CR>
" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %
